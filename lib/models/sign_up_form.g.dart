// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_up_form.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignUpForm _$SignUpFormFromJson(Map<String, dynamic> json) {
  return SignUpForm(
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    email: json['email'] as String,
    state: json['state'] as String,
    city: json['city'] as String,
    golfCourse: json['golfCourse'] as String,
    phoneNumber: json['phoneNumber'] as String,
    password: json['password'] as String,
  );
}

Map<String, dynamic> _$SignUpFormToJson(SignUpForm instance) =>
    <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'email': instance.email,
      'state': instance.state,
      'city': instance.city,
      'golfCourse': instance.golfCourse,
      'phoneNumber': instance.phoneNumber,
      'password': instance.password,
    };
