import 'package:json_annotation/json_annotation.dart';

part 'sign_up_form.g.dart';

@JsonSerializable()
class SignUpForm {
  String firstName;
  String lastName;
  String email;
  String state;
  String city;
  String golfCourse;
  String phoneNumber;
  String password;

  SignUpForm({
    this.firstName,
    this.lastName,
    this.email,
    this.state,
    this.city,
    this.golfCourse,
    this.phoneNumber,
    this.password,
  });

  factory SignUpForm.fromJson(Map<String, dynamic> json) => _$SignUpFormFromJson(json);

  Map<String, dynamic> toJson() => _$SignUpFormToJson(this);
}
