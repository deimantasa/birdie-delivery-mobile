import 'package:flutter/material.dart';

import 'sign_up_contract.dart';
import 'sign_up_model.dart';
import 'sign_up_presenter.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> implements View {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  SignUpModel _signUpModel;
  SignUpPresenter _signUpPresenter;

  @override
  void initState() {
    _signUpModel = SignUpModel();
    _signUpPresenter = SignUpPresenter(this, context, _signUpModel);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  @override
  void showMessage(String message) {
    print("$message");
  }

  @override
  void updateView() {
    if (mounted) setState(() {});
  }

  Widget _buildBody() {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.green, Colors.lightGreen],
          ),
        ),
        child: _signUpModel.signUpLoadingState.isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: [
                  AppBar(
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    actions: [
                      IconButton(
                        icon: Icon(Icons.refresh),
                        onPressed: () => _signUpPresenter.resetForm(),
                      )
                    ],
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Form(
                        key: _formKey,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 50),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(child: Container(width: 300, height: 200, child: FlutterLogo())),
                              SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                decoration: InputDecoration(labelText: "First Name"),
                                textCapitalization: TextCapitalization.words,
                                onChanged: (input) => _signUpModel.signUpForm.firstName = input,
                                validator: (input) => input.isNotEmpty ? null : "First Name cannot be empty",
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                decoration: InputDecoration(labelText: "Last Name"),
                                textCapitalization: TextCapitalization.words,
                                onChanged: (input) => _signUpModel.signUpForm.lastName = input,
                                validator: (input) => input.isNotEmpty ? null : "Last Name cannot be empty",
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                decoration: InputDecoration(labelText: "E-Mail Address"),
                                textCapitalization: TextCapitalization.none,
                                keyboardType: TextInputType.emailAddress,
                                onChanged: (input) => _signUpModel.signUpForm.email = input,
                                validator: (input) => input.isNotEmpty && input.contains("@") ? null : "E-Mail is not correct",
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                decoration: InputDecoration(labelText: "Re-enter E-Mail Address"),
                                textCapitalization: TextCapitalization.none,
                                keyboardType: TextInputType.emailAddress,
                                validator: (input) => input == _signUpModel.signUpForm.email ? null : "E-Mails doesn't match",
                              ),
                              SizedBox(
                                height: 24,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("State"),
                                      Container(
                                        padding: EdgeInsets.symmetric(horizontal: 8),
                                        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                        child: DropdownButton<String>(
                                          underline: SizedBox(),
                                          value: _signUpModel.signUpForm.state,
                                          items: <String>['A', 'B', 'C', 'D'].map((String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (selectedState) => _signUpPresenter.updateState(selectedState),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("City"),
                                      Container(
                                        padding: EdgeInsets.symmetric(horizontal: 8),
                                        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                        child: DropdownButton<String>(
                                          underline: SizedBox(),
                                          value: _signUpModel.signUpForm.city,
                                          items: <String>['Aa', 'Bb', 'Cc', 'Dd'].map((String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (selectedCity) => _signUpPresenter.updateCity(selectedCity),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                decoration: InputDecoration(labelText: "Golf Course"),
                                textCapitalization: TextCapitalization.sentences,
                                onChanged: (input) => _signUpModel.signUpForm.golfCourse = input,
                                validator: (input) => input.isNotEmpty ? null : "Golf Course cannot be empty",
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                decoration: InputDecoration(labelText: "Phone Number"),
                                textCapitalization: TextCapitalization.sentences,
                                keyboardType: TextInputType.phone,
                                onChanged: (input) => _signUpModel.signUpForm.phoneNumber = input,
                                validator: (input) => input.isNotEmpty ? null : "Phone Number cannot be empty",
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                decoration: InputDecoration(labelText: "Create Password"),
                                textCapitalization: TextCapitalization.sentences,
                                obscureText: true,
                                onChanged: (input) => _signUpModel.signUpForm.password = input,
                                validator: (input) => input.isNotEmpty ? null : "Password cannot be empty",
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              TextFormField(
                                decoration: InputDecoration(labelText: "Re-enter Password"),
                                textCapitalization: TextCapitalization.sentences,
                                obscureText: true,
                                validator: (input) =>
                                    input == _signUpModel.signUpForm.password ? null : "Passwords doesn't match",
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              InkWell(
                                onTap: () => showMessage('Go to Terms and Services page'),
                                child: Text.rich(
                                  TextSpan(
                                    text: 'By signing up you are agreeing to our ',
                                    style: Theme.of(context).textTheme.bodyText1,
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Terms and Services',
                                          style: TextStyle(decoration: TextDecoration.underline, fontWeight: FontWeight.bold)),
                                      // can add more TextSpans here...
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              InkWell(
                                onTap: () => showMessage('Go to Privacy Policy page'),
                                child: Text.rich(
                                  TextSpan(
                                    text: 'View our ',
                                    style: Theme.of(context).textTheme.bodyText1,
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Privacy Policy',
                                          style: TextStyle(decoration: TextDecoration.underline, fontWeight: FontWeight.bold)),
                                      // can add more TextSpans here...
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Container(
                                width: double.maxFinite,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Agree and Sign Up",
                                      style: TextStyle(fontSize: 24, fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                  onPressed: () => _validateForm(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  void _validateForm() {
    if (_formKey.currentState.validate()) {
      _signUpPresenter.signUpUser();
    }
  }

  @override
  void resetForm() {
    _formKey.currentState.reset();
    updateView();
  }
}
