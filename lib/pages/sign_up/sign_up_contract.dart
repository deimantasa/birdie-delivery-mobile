abstract class Presenter{
	Future<void> signUpUser();
	void resetForm();
	void updateState(String selectedState);
	void updateCity(String selectedState);
}

abstract class View{
	void updateView();
	void showMessage(String message);
	void resetForm();
}