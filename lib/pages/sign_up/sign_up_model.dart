import 'package:birdie_delivery/models/loading_state.dart';
import 'package:birdie_delivery/models/sign_up_form.dart';

class SignUpModel {
  LoadingState signUpLoadingState = LoadingState.notLoading();

  SignUpForm signUpForm = SignUpForm();

  void resetForm() {
    signUpForm = SignUpForm();
  }
}
