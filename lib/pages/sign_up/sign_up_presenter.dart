import 'package:flutter/material.dart';

import 'sign_up_contract.dart';
import 'sign_up_model.dart';

class SignUpPresenter implements Presenter {
  View _view;
  SignUpModel _signUpModel;

  SignUpPresenter(View view, BuildContext context, SignUpModel signUpModel) {
    _view = view;
    _signUpModel = signUpModel;
  }

  @override
  Future<void> signUpUser() async {
    _signUpModel.signUpLoadingState.isLoading = true;
    _view.updateView();

    print(_signUpModel.signUpForm.toJson());
    //Your http call goes here
    await Future.delayed(Duration(seconds: 3));

    _signUpModel.signUpLoadingState.isLoading = false;
    _view.updateView();
  }

  @override
  void resetForm() {
    _signUpModel.resetForm();
    _view.resetForm();
  }

  @override
  void updateState(String selectedState) {
    _signUpModel.signUpForm.state = selectedState;
    _view.updateView();
  }

  @override
  void updateCity(String selectedCity) {
    _signUpModel.signUpForm.city = selectedCity;
    _view.updateView();
  }
}
